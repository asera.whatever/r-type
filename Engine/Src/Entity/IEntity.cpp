/*
** EPITECH PROJECT, 2018
** LOL
** File description:
** singe
*/

#include <mutex>
#include "IEntity.hpp"

int IEntity::makeId()
{
	static int i = 0;
	i++;
	return i - 1;
}

IEntity::IEntity() :
_id(makeId()),
_streamTimer(),
_entityType(IENTITY_ID)
{
}

int IEntity::getId() const
{
	return _id;
}

void IEntity::setId(int id)
{
    _id = id;
}

int IEntity::getStreamTimer()
{
    return _streamTimer;
}

void IEntity::incStreamTimer()
{
    _streamTimer++;
}

void IEntity::resetStreamTimer()
{
	_streamTimer = 0;
}

int IEntity::getEntityType() const
{
	return _entityType;
}

void IEntity::setEntityType(int type)
{
	_entityType = type;
}