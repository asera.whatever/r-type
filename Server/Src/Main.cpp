//
// Created by reimua on 27/09/18.
//

#include <Asteroid.hpp>
#include "Server.hpp"
#include "Player.hpp"
#include "Core.hpp"
#include "ClassicMonster.hpp"

int main(int argc, char **argv)
{
	if (argc != 2)
		return 84;
	Server::start(atoi(argv[1]));
}