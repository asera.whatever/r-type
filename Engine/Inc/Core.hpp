/*
** EPITECH PROJECT, 2018
** LOL
** File description:
** singe
*/

#pragma once

#include <vector>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <mutex>

#include "ACore.hpp"
#include "IText.hpp"
#include "ISprite.hpp"
#include "AudioManager.hpp"
#include "ActionManager.hpp"
#include "NetworkManager.hpp"
#include "PhysicManager.hpp"

class Core : public ACore {
private:
	int _frameRate;

	sf::Vector2i		_screenSize;

	std::vector<ISprite *> _sprites;
	std::vector<IEntity *> _entities;
	std::vector<IEntity *> _deletionQueue;
	std::vector<int> _topQueue;

	sf::RenderWindow	*_window;
	IAudioManager *_audioManager;
	IActionManager *_actionManager;
	IPhysicManager *_physicManager;
	INetworkManager *_networkManager;

	bool _canRender;
	bool _canFeed;
	bool _isServer;

    void procDelectionQueue();

public:
	Core(sf::Vector2i &screenSize);
	~Core();

	void feedEntity(IEntity *) override;
	void feedEntity(ISprite *) override;
	void addToDeletionQueue(IEntity *) override;
	void addToDeletionQueueById(int) override;

	void run();
	void updateEntities();
	void renderEntities();

	void authorizeFeed(bool) override;
	void authorizeRender(bool) override;
	void setServerStatus(bool) override;

	void streamEntityFeed(IEntity *);

	IAudioManager *getAudioManager() override;
	IActionManager *getActionManager() override;
	sf::RenderWindow *getRenderWindow() override;
	INetworkManager *getNetworkManager() override;
	IPhysicManager *getPhysicManager() override;

	std::vector<IEntity*> getEntities();

	void setActionManager(IActionManager *) override;
	void setPhysicManager(IPhysicManager *) override;
	void setNetworkManager(INetworkManager *) override;
	void setAudioManager(IAudioManager *_audioManager) override;


	void handleWindowEvent();
	sf::Vector2i getScreenSize() const override;

	float absoluteToRelativeX(float) override;
	float absoluteToRelativeY(float) override;
    float relativeToAbsoluteX(float) override;
    float relativeToAbsoluteY(float) override;

    IEntity *getEntityFromId(int) override;
	void setOnTop(int id) override;
	void procTopQueue();
};
