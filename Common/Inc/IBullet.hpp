//
// Created by reimua on 27/09/18.
//

#pragma once

#include "ISprite.hpp"

#define IBULLET_ID 4

/*
 * IBullet define a generic interface that can be used for all subsequent bullet.
 */

class IBullet : public ISprite {
public:
    IBullet(sf::Vector2f &pos, std::string texturePath);
    virtual ~IBullet();
    void update(ACore &core) override;
    virtual void updateBullet(ACore &core) = 0;
    void incStreamTimer() override;
};
