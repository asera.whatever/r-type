//
// Created by reimua on 27/09/18.
//

#pragma once

#include "ISprite.hpp"

#define PLAYER_ID 3

class Player : public ISprite {
private:
	int _health;
	int _playerNbr;
	int _level;
public:
    Player(sf::Vector2f &pos, int playerNbr = 0);
    virtual ~Player();
    void update(ACore &core) override;
    void onCollision(ACore&, ISprite *) override;

    void handleInput(ACore &);
    void fixPosition(ACore &);
    sf::Packet getEntityPacket(network::PACKET_TYPE) override;
    void incStreamTimer() override;
};

namespace NetworkConstructor {
    IEntity *createPlayerFromPacket(ACore &core, sf::Packet packet);
};