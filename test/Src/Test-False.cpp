/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#include <stdexcept>
#include <criterion/criterion.h>
#include "False.hpp"

Test(False, compute)
{
	False _false;

	cr_assert(_false.compute(1) == false);
}

Test(False, computeError)
{
	False _false;

	try {
		_false.compute(2);
		cr_assert(0);
	} catch (const std::exception &e) {
		(void)e;
	}
}

Test(False, getPinType)
{
	False _false;

	cr_assert(_false.getPinType(1) == nts::TRANSMITTER);
}

Test(False, allSetLinkCase)
{
	False _false;

	try {
		_false.setLink(2, _false, 1);
		cr_assert(0);
	} catch (const std::exception &e) {
		(void)e;
	}
	_false.setLink(1, _false, 1);
	try {
		_false.setLink(1, _false, 1);
		cr_assert(0);
	} catch (const std::exception &e) {
		(void)e;
	}
}