//
// Created by reimua on 27/11/18.
//

#pragma once

#include "ISprite.hpp"

#define BLACKSTAR_ID 10

class BlackStar : public ISprite {
private:
	int _counter;
	int _health;
	void burst(ACore&);
public:
	BlackStar(sf::Vector2f &pos);
	virtual ~BlackStar();
	void update(ACore &core) override;
	void onCollision(ACore&, ISprite *) override;

	sf::Packet getEntityPacket(network::PACKET_TYPE) override;
	void incStreamTimer() override;
};

namespace NetworkConstructor {
	IEntity *createBlackStarFromPacket(ACore &core, sf::Packet packet);
};