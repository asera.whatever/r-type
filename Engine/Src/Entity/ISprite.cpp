/*
** EPITECH PROJECT, 2018
** LOL
** File description:
** singe
*/

#include <ISprite.hpp>

#include "NetworkManager.hpp"
#include "ISprite.hpp"
#include "Core.hpp"

ISprite::ISprite(sf::Vector2f &pos, std::string texturePath, float scale, enum physicType pt) :
IEntity(),
_texturePath(texturePath),
_sprite(),
_pos(pos),
_texture(),
_physicType(pt),
_packetNbr(0)
{
    _entityType = ISPRITE_ID;
	_texture.loadFromFile(texturePath);
    _sprite.setScale(scale, scale);
	_sprite.setTexture(_texture);
    _sprite.setOrigin(_texture.getSize().x / 2, _texture.getSize().y / 2);
}

sf::Sprite ISprite::getSprite() const
{
	return _sprite;
}

enum physicType ISprite::getPhysicType() const
{
    return _physicType;
}

void ISprite::render(ACore &core)
{
	sf::Vector2f spritePos;

	spritePos.x = core.absoluteToRelativeX(_pos.x);
	spritePos.y = core.absoluteToRelativeY(_pos.y);
 	_sprite.setPosition(spritePos);
 	if (core.getRenderWindow() != nullptr)
    	core.getRenderWindow()->draw(_sprite);
}

sf::Packet ISprite::getEntityPacket(network::PACKET_TYPE packetType)
{
	sf::Packet packet;

	packet << packetType;
    if (packetType == network::ENT_UPDATE)
        packet << _id;
	packet << _entityType << _id << _texturePath << _pos.x << _pos.y << _physicType;
    if (packetType == network::ENT_UPDATE) {
        packet << _packetNbr++;
    }
    return packet;
}

void ISprite::updateFromPacket(sf::Packet packet)
{
    int id;
    std::string texturePath;
    float posX;
    float posY;
    unsigned int packetNbr;
    packet >> id;
    packet >> texturePath;
    packet >> posX;
    packet >> posY;
    packet >> packetNbr;

    _pos.x = posX;
    _pos.y = posY;
    _packetNbr = packetNbr;
}

void ISprite::setSprite(sf::Sprite sprite)
{
	_sprite = sprite;
}
