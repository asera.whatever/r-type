//
// Created by reimua on 25/11/18.
//

#pragma once

#include <string>
#include <SFML/Audio.hpp>

/*
 * IAudioManager
 *
 * PlayBgm:
 *      Play a background music on loop. only 1 bgm can be played at a time.
 * StopBgm:
 *      Stop the current Bgm.
 * PlaySound:
 *      Play a sound. Multiple sound can be Played at the same time.
 */

class IAudioManager {
public:
    IAudioManager(){};
    virtual ~IAudioManager(){};
    virtual void stopBgm() = 0;
    virtual bool playBgm(const std::string name) = 0;
    virtual bool playSound(const std::string name) = 0;
};
