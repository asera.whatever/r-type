//
// Created by reimua on 25/11/18.
//

#pragma once

#include <SFML/Graphics.hpp>

class IActionManager {
public:
	IActionManager(){};
	virtual ~IActionManager(){};

	virtual bool isKeyPressed(sf::Keyboard::Key key) = 0;
	virtual bool isKeyReleased(sf::Keyboard::Key key) = 0;
	virtual bool isKeyDown(sf::Keyboard::Key key) = 0;
	virtual bool isKeyUp(sf::Keyboard::Key key) = 0;
	virtual bool isKeyMapped(sf::Keyboard::Key key) = 0;
	virtual std::vector<sf::Keyboard::Key> getPressedKey() = 0;
	virtual void flush() = 0;

	virtual void setFocus(bool focus) = 0;
};