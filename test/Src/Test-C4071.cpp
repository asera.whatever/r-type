/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#include <stdexcept>
#include <criterion/criterion.h>
#include "C4071.hpp"
#include "Input.hpp"

Test(C4071, computeUndefinedPin)
{
	C4071 _c4071;

	for (int i = 0; i < 14; i++)
		cr_assert_eq(_c4071.compute(i + 1), (-true));
}

Test(C4071, getPinType)
{
	C4071 _c4071;
	nts::PinType type;

	for (int i = 1; i < 15; i++) {
		if (i == 3 || i == 4 || i == 11 || i == 10)
			type = nts::TRANSMITTER;
		else if (i == 7 || i == 14)
			type = nts::NONE;
		else
			type = nts::RECEIVER;
		cr_assert_eq(_c4071.getPinType(i), type);
	}
}

Test(C4071, allSetLinkCase)
{
	C4071 _c4071;

	try {
		_c4071.setLink(15, _c4071, 1);
		cr_assert(0);
	} catch (const std::exception &e) {
		(void)e;
	}
	_c4071.setLink(1, _c4071, 1);
	try {
		_c4071.setLink(1, _c4071, 1);
		cr_assert(0);
	} catch (const std::exception &e) {
		(void)e;
	}
}

Test(C4071, computeAllCaseGate1)
{
	Input i1;
	Input i2;
	C4071 _c4071;

	_c4071.setLink(1, i1, 1);
	_c4071.setLink(2, i2, 1);
	i1.setLink(1, _c4071, 1);
	i2.setLink(1, _c4071, 2);
	cr_assert_eq(_c4071.compute(3), (-true));
	i1.setValue(1);
	cr_assert_eq(_c4071.compute(3), (true));
	i2.setValue(1);
	cr_assert_eq(_c4071.compute(3), (true));
	i2.setValue(0);
	cr_assert_eq(_c4071.compute(3), (true));
	i1.setValue(0);
	cr_assert_eq(_c4071.compute(3), (false));
}

Test(C4071, computeAllCaseGate2)
{
	Input i1;
	Input i2;
	C4071 _c4071;

	_c4071.setLink(5, i1, 1);
	_c4071.setLink(6, i2, 1);
	i1.setLink(1, _c4071, 5);
	i2.setLink(1, _c4071, 6);
	cr_assert_eq(_c4071.compute(4), (-true));
	i1.setValue(1);
	cr_assert_eq(_c4071.compute(4), (true));
	i2.setValue(1);
	cr_assert_eq(_c4071.compute(4), (true));
	i2.setValue(0);
	cr_assert_eq(_c4071.compute(4), (true));
	i1.setValue(0);
	cr_assert_eq(_c4071.compute(4), (false));
}

Test(C4071, computeAllCaseGate3)
{
	Input i1;
	Input i2;
	C4071 _c4071;

	_c4071.setLink(8, i1, 1);
	_c4071.setLink(9, i2, 1);
	i1.setLink(1, _c4071, 8);
	i2.setLink(1, _c4071, 9);
	cr_assert_eq(_c4071.compute(10), (-true));
	i1.setValue(1);
	cr_assert_eq(_c4071.compute(10), (true));
	i2.setValue(1);
	cr_assert_eq(_c4071.compute(10), (true));
	i2.setValue(0);
	cr_assert_eq(_c4071.compute(10), (true));
	i1.setValue(0);
	cr_assert_eq(_c4071.compute(10), (false));
}

Test(C4071, computeAllCaseGate4)
{
	Input i1;
	Input i2;
	C4071 _c4071;

	_c4071.setLink(13, i1, 1);
	_c4071.setLink(12, i2, 1);
	i1.setLink(1, _c4071, 13);
	i2.setLink(1, _c4071, 12);
	cr_assert_eq(_c4071.compute(11), (-true));
	i1.setValue(1);
	cr_assert_eq(_c4071.compute(11), (true));
	i2.setValue(1);
	cr_assert_eq(_c4071.compute(11), (true));
	i2.setValue(0);
	cr_assert_eq(_c4071.compute(11), (true));
	i1.setValue(0);
	cr_assert_eq(_c4071.compute(11), (false));
}