//
// Created by kaww on 27/09/18.
//

#pragma once

#include "IMonster.hpp"

#define CLASSICMONSTER_ID 21

class ClassicMonster : public IMonster {
private:
	int _health;
public:
    ClassicMonster(sf::Vector2f &pos, float speed, float amplitude, float amplitudeSpeed, float scale = 1);
    ~ClassicMonster();
    void updateMonster(ACore &Acore) override;
    void onCollision(ACore &, ISprite *) override;

    sf::Packet getEntityPacket(network::PACKET_TYPE) override;
    void updateFromPacket(sf::Packet packet) override;
};

namespace NetworkConstructor {
    IEntity *createClassicMonsterFromPacket(ACore &core, sf::Packet packet);
};