//
// Created by kaww on 27/09/18.
//

#include "PlayerBullet.hpp"
#include "ActionManager.hpp"
#include "SpeedMonster.hpp"

SpeedMonster::SpeedMonster(sf::Vector2f &pos, float scale) :
IMonster(pos, "./Assets/Sprites/SpeedMonster.png", 1, 5, 0.03, scale),
_health(5)

{
    _entityType = SPEEDMONSTER_ID;
}

SpeedMonster::~SpeedMonster()
{
}

void SpeedMonster::updateMonster(ACore &core) {
    if (_health < 0) {
        core.addToDeletionQueue(this);
        auto monsterPos = sf::Vector2f(100, 0);
            auto monster = new SpeedMonster(monsterPos, 4);
            core.feedEntity(monster);
    }
}

void SpeedMonster::onCollision(ACore &core, ISprite *sprite)
{
    if (sprite->getEntityType() == PLAYERBULLET_ID) {
		_health--;
        core.addToDeletionQueue(sprite);
    }
}

sf::Packet SpeedMonster::getEntityPacket(network::PACKET_TYPE packetType)
{
    sf::Packet packet;

    packet << packetType;
    if (packetType == network::ENT_UPDATE) {
        packet << _id;
    }
    packet << SPEEDMONSTER_ID << _id;
    packet << _pos.x << _pos.y << _scale << _originalY << _counter;
    return packet;
}

void SpeedMonster::updateFromPacket(sf::Packet packet)
{
    int id;
    std::string texturePath;
    float posX;
    float posY;

    packet >> id;
    packet >> posX;
    packet >> posY;
    _pos.x = posX;
    _pos.y = posY;
}

IEntity *NetworkConstructor::createSpeedMonsterFromPacket(ACore &core, sf::Packet packet)
{
    int id;
    sf::Vector2f pos;
    float scale;
    float originalY;
    float counter;

    packet >> id >> pos.x >> pos.y >> scale >> originalY >> counter;

    IMonster *entity = new SpeedMonster(pos, scale);
    entity->setOriginalY(originalY);
    entity->setCounter(counter);
    entity->setId(id);
    return entity;
}