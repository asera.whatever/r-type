//
// Created by reimua on 19/11/18.
//

#include <iostream>
#include <stdexcept>
#include <NetworkManager.hpp>

#include "Player.hpp"
#include "NetworkManager.hpp"
#include "../../Common/Inc/PlayerBullet.hpp"

NetworkManager::NetworkManager() :
_socket(),
_ipTarget(),
_portTarget(),
_clients(),
_binded(false),
_playerJoinCallback(nullptr)
{
    _socket.setBlocking(false);
}

NetworkManager::~NetworkManager()
{}

void NetworkManager::bindSocket(unsigned short port)
{
    if (_socket.bind(port) != sf::Socket::Done)
        throw std::invalid_argument("Invalid port number");
    _binded = true;
    std::cout << "NetworkManager is listening to port " << port << std::endl;
}

void NetworkManager::readSocket(ACore &core)
{
    // Receiver variable
    sf::Packet packet;
    sf::IpAddress sender;
    unsigned short senderPort;


    for (auto &it: _clients) {
        for (int i = 0; i < sf::Keyboard::KeyCount; i++) {
            it._keyMap[i] = 0;
        }
    }

    auto state = _socket.receive(packet, sender, senderPort);
    if (state == sf::Socket::NotReady)
        return;

    while (state == sf::Socket::Done) {
        int opCode;
        int objectId;
        int id;

        packet >> opCode;
        auto networkCode = static_cast<enum  network::PACKET_TYPE>(opCode);

        // A new player connected ?
        if (networkCode == network::PLAYER_JOIN && _clients.size() < 4) {
			if (_playerJoinCallback)
			    _playerJoinCallback(core, sender, senderPort);
			std::cout << "Someone joined with " << sender << ":" << senderPort << std::endl;
        }
        if (networkCode == network::PORT_REDIRECTION) {
            packet >> _portTarget;
            sf::Packet answer;
            answer << network::PLAYER_JOIN;
            this->sendPacket(answer, sender, _portTarget);
        }
        // An entity was created on the server side.
        if (networkCode == network::ENT_CREATION) {
            packet >> objectId;
            core.feedEntity(_constructor[objectId](core, packet));
        }
        // An entity on the client side need to be updated
        if (networkCode == network::ENT_UPDATE) {
            packet >> id;
            packet >> objectId;
            IEntity *target = core.getEntityFromId(id);
            if (target == nullptr) {
            	core.feedEntity(_constructor[objectId](core, packet));
            } else {
                target->updateFromPacket(packet);
            }
        }
        // An entity was destroyed on the server side
        if (networkCode == network::ENT_DESTRUCTION) {
            packet >> id;
            core.addToDeletionQueueById(id);
        }
        // We received an input from a client.
        if (networkCode == network::INPUT) {
            int key;
            packet >> key;
            for (auto &it: _clients) {
                if (sender == it.ip && senderPort == it.port && key < sf::Keyboard::KeyCount) {
                    it._keyMap[key]++;
                }
            }
        }
        // Stream end, process should stop, although exit is the lazy way.
        if (networkCode == network::STREAM_END)
            exit(0);
        state = _socket.receive(packet, sender, senderPort);
    }
}

void NetworkManager::streamEntityUpdate(IEntity *entity) {
    sf::Packet packet = entity->getEntityPacket(network::ENT_UPDATE);
    for (auto &it: _clients) {
        _socket.send(packet, it.ip, it.port);
    }
}

void NetworkManager::streamEntityCreation(IEntity *entity)
{
    sf::Packet packet = entity->getEntityPacket(network::ENT_CREATION);
    for (auto &it: _clients)
        _socket.send(packet, it.ip, it.port);
}

void NetworkManager::streamEntityDestruction(IEntity *entity)
{
    sf::Packet packet;
    packet << network::ENT_DESTRUCTION << entity->getId();
    for (auto &it: _clients)
        _socket.send(packet, it.ip, it.port);
    for (auto &it: _clients)
        _socket.send(packet, it.ip, it.port);
}

void NetworkManager::streamInput(IActionManager *actionManager)
{
    auto pressedKey = actionManager->getPressedKey();

    for (const auto &keyCode: pressedKey) {
        sf::Packet packet;

        packet << network::INPUT;
        packet << keyCode;
        this->sendPacket(packet, _ipTarget, _portTarget);
    }
}

void NetworkManager::sendPacket(sf::Packet packet, sf::IpAddress recipient, unsigned short port)
{
    _socket.send(packet, recipient, port);
}

void NetworkManager::addNetworkConstructor(int objectID, IEntity *(constructor)(ACore &, sf::Packet))
{
    _constructor[objectID] = constructor;
}

bool NetworkManager::isClientKeyPressed(int clientNbr, sf::Keyboard::Key key)
{
    if (clientNbr >= _clients.size())
        return false;
    return _clients[clientNbr]._keyMap[key % sf::Keyboard::KeyCount] != 0;
}

void NetworkManager::endStream()
{
	sf::Packet packet;
	packet << network::STREAM_END;
	for (auto &it: _clients)
		_socket.send(packet, it.ip, it.port);
}

void NetworkManager::setPlayerJoinCallback(void (*callback)(ACore&, sf::IpAddress sender, unsigned short senderPort))
{
    _playerJoinCallback = callback;
}

std::vector<client_s> &NetworkManager::getClientVector()
{
	return _clients;
}
