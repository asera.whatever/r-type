//
// Created by reimuanal on 27/09/18.
//

#pragma once

#include "IBullet.hpp"

#define BLACKSTARBULLET_ID 11

/*
 * PlayerBullet define the implementation of bullet shot by any player.
 */

class BlackStarBullet : public IBullet {
	float _counter;
	float _originalY;
	float _amplitude;
public:
	BlackStarBullet(sf::Vector2f &pos, float amplitude = 1, int counter = 0);
	virtual ~BlackStarBullet(){};
	void updateBullet(ACore &core) override;
	void onCollision(ACore &core, ISprite *) override;
	sf::Packet getEntityPacket(network::PACKET_TYPE) override;
};

namespace NetworkConstructor {
	IEntity *createBlackStarBulletFromPacket(ACore &core, sf::Packet);
};