//
// Created by reimua on 27/09/18.
//

#include <PowerUp.hpp>
#include <Asteroid.hpp>
#include <HealthUp.hpp>
#include "ActionManager.hpp"
#include "IText.hpp"
#include "Player.hpp"
#include "ClassicMonster.hpp"
#include "MonsterBullet.hpp"
#include "NetworkManager.hpp"
#include "BlackStar.hpp"
#include "PlayerBullet.hpp"
#include "AudioManager.hpp"

Player::Player(sf::Vector2f &pos, int playerNbr) :
ISprite(pos, "./Assets/Sprites/Player" + std::to_string(playerNbr % 4 + 1) + ".png", 1, PT_COLLIDER),
_playerNbr(playerNbr),
_health(20),
_level(0)
{

    _entityType = PLAYER_ID;
}

Player::~Player()
{
}

void Player::handleInput(ACore &core)
{
    sf::Vector2f newPos = _pos;
    auto networkManager = core.getNetworkManager();

    if (networkManager->isClientKeyPressed(_playerNbr, sf::Keyboard::Up))
        newPos.y -= 2;
    if (networkManager->isClientKeyPressed(_playerNbr, sf::Keyboard::Down))
        newPos.y += 2;
    if (networkManager->isClientKeyPressed(_playerNbr, sf::Keyboard::Right))
        newPos.x += 1;
    if (networkManager->isClientKeyPressed(_playerNbr, sf::Keyboard::Left))
        newPos.x -= 1;
    _pos = newPos;
    if (networkManager->isClientKeyPressed(_playerNbr, sf::Keyboard::W)) {
        if (_level == 0)
			core.feedEntity(new PlayerBullet(newPos, 0));
    	else if (_level == 1) {
			core.feedEntity(new PlayerBullet(newPos));
			core.feedEntity(new PlayerBullet(newPos, -1));
		} else {
			core.feedEntity(new PlayerBullet(newPos, 0));
			for (int i = 1; i < 3; i++) {
				core.feedEntity(new PlayerBullet(newPos, i));
				core.feedEntity(new PlayerBullet(newPos, -i));
			}
		}
    }
}

void Player::fixPosition(ACore &core)
{
    if (_pos.x < -100 + 6)
        _pos.x = -100 + 6;
    if (_pos.y < -100 + 9.5)
        _pos.y = -100 + 9.5f;
    if (_pos.x > 100 - 6)
        _pos.x = 100 - 6;
    if (_pos.y > 100 - 9.5)
        _pos.y = 100 - 9.5f;
}

void Player::update(ACore &core)
{
    this->handleInput(core);
    this->fixPosition(core);
    if (_health < 0)
    	core.addToDeletionQueue(this);
}

void Player::onCollision(ACore &core, ISprite *sprite)
{
	if (sprite->getEntityType() == MONSTERBULLET_ID) {
		_health--;
		core.addToDeletionQueue(sprite);
	}
	if (sprite->getEntityType() == CLASSICMONSTER_ID) {
		_health -= 10;
		core.addToDeletionQueue(sprite);
	}
	if (sprite->getEntityType() == BLACKSTAR_ID || sprite->getEntityType() == ASTEROID_ID) {
		// you dead lol
        // mdr t un ouf toi
		_health = -100000;
	}
	if (sprite->getEntityType() == POWERUP_ID) {
		_level++;
		core.addToDeletionQueue(sprite);
	}
    if (sprite->getEntityType() == HEALTHUP_ID) {
        _health += 20;
        core.addToDeletionQueue(sprite);
    }
}

void Player::incStreamTimer()
{
    _streamTimer += 15;
}

sf::Packet Player::getEntityPacket(network::PACKET_TYPE packetType)
{
	sf::Packet packet;

	packet << packetType;
	if (packetType == network::ENT_UPDATE)
		packet << _id;
	packet << _entityType << _id << _texturePath << _pos.x << _pos.y << _playerNbr;
	if (packetType == network::ENT_UPDATE) {
		packet << _packetNbr++;
	}
	return packet;
}

IEntity *NetworkConstructor::createPlayerFromPacket(ACore &core, sf::Packet packet)
{
    int id;
    int playerNbr;
    sf::Vector2f pos;
    std::string textPath;

    packet >> id >> textPath >> pos.x >> pos.y >> playerNbr;

    ISprite *sprite = new Player(pos, playerNbr);


    sprite->setId(id);
    return sprite;
}