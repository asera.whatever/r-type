/*
** EPITECH PROJECT, 2018
** LOL
** File description:
** singe
*/

#pragma once

#include <SFML/Graphics.hpp>

class IEntity;
class ISprite;
class IAudioManager;
class IActionManager;
class IPhysicManager;
class INetworkManager;

class ACore {
public:
	ACore() = default;
	virtual ~ACore() = default;

	virtual IAudioManager *getAudioManager() = 0;
	virtual IActionManager *getActionManager() = 0;
	virtual sf::Vector2i getScreenSize() const = 0;
    virtual sf::RenderWindow *getRenderWindow() = 0;
    virtual INetworkManager *getNetworkManager() = 0;
	virtual IPhysicManager *getPhysicManager() = 0;

	virtual void setActionManager(IActionManager *) = 0;
	virtual void setPhysicManager(IPhysicManager *) = 0;
	virtual void setNetworkManager(INetworkManager *) = 0;
	virtual void setAudioManager(IAudioManager *_audioManager) = 0;

	virtual void feedEntity(IEntity *) = 0;
	virtual void feedEntity(ISprite *) = 0;

    virtual void addToDeletionQueue(IEntity *) = 0;
    virtual void addToDeletionQueueById(int) = 0;
    virtual void authorizeRender(bool) = 0;

	virtual void authorizeFeed(bool) = 0;
	virtual void setServerStatus(bool) = 0;
	virtual float absoluteToRelativeX(float) = 0;

	virtual float absoluteToRelativeY(float) = 0;
	virtual float relativeToAbsoluteX(float) = 0;
	virtual float relativeToAbsoluteY(float) = 0;

	virtual IEntity *getEntityFromId(int) = 0;

	virtual void setOnTop(int id) = 0;
};
