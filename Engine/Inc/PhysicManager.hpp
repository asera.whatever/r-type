//
// Created by reimua on 21/11/18.
//

#pragma once

#include "ACore.hpp"
#include "IPhysicManager.hpp"

class PhysicManager final : public IPhysicManager {
public:
    PhysicManager();
    ~PhysicManager();

    bool spriteCollision(ISprite *, ISprite *);
    void processCollision(ACore&, std::vector<ISprite*>, std::vector<ISprite*>);
    void detectCollision(ACore&, std::vector<ISprite*>) override;
};