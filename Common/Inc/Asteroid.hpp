//
// Created by reimua on 28/11/18.
//

#pragma once

#include "ISprite.hpp"

#define ASTEROID_ID 14

class Asteroid : public ISprite {
private:
	int _spriteNbr;
	int _health;
	sf::Vector2f _originalPos;
	sf::Vector2f _target;
public:
	Asteroid(sf::Vector2f &pos, int spriteNbr = 1);
	virtual ~Asteroid();
	void update(ACore &core) override;
	void onCollision(ACore&, ISprite *) override;

	sf::Packet getEntityPacket(network::PACKET_TYPE) override;
	void incStreamTimer() override;
};

namespace NetworkConstructor {
	IEntity *createAsteroidFromPacket(ACore &core, sf::Packet packet);
};