//
// Created by reimua on 21/11/18.
//

#include "PhysicManager.hpp"

PhysicManager::PhysicManager() {}
PhysicManager::~PhysicManager() {}

bool PhysicManager::spriteCollision(ISprite *collider, ISprite *other)
{
    sf::FloatRect colliderRect = collider->getSprite().getGlobalBounds();
    sf::FloatRect otherRect = other->getSprite().getGlobalBounds();

    return colliderRect.contains(otherRect.left, otherRect.top) ||
           colliderRect.contains(otherRect.left + otherRect.width, otherRect.top) ||
           colliderRect.contains(otherRect.left, otherRect.top + otherRect.height) ||
           colliderRect.contains(otherRect.left + otherRect.width, otherRect.top + otherRect.height);
}

void PhysicManager::processCollision(ACore &core, std::vector<ISprite *> collider, std::vector<ISprite *> otherSet)
{
    for (auto const &current: collider) {
        for (auto const &other: otherSet) {
            if (current->getId() == other->getId())
                continue;
            if (this->spriteCollision(current, other) || this->spriteCollision(other, current)) {
                current->onCollision(core, other);
                other->onCollision(core, current);
            }
        }
    }
}

void PhysicManager::detectCollision(ACore &core, std::vector<ISprite *> sprites)
{
    std::vector<ISprite *> collider;
    std::vector<ISprite *> lazy;

    for (unsigned int i = 0; i < sprites.size(); i++) {
        if (sprites[i]->getPhysicType() == PT_LAZY)
            lazy.push_back(sprites[i]);
        else if (sprites[i]->getPhysicType() == PT_COLLIDER)
            collider.push_back(sprites[i]);
    }
    this->processCollision(core, collider, collider);
    this->processCollision(core, collider, lazy);
}