//
// Created by reimua on 25/11/18.
//

#pragma once

#include "IEntity.hpp"

#IEVENT_ID 100

class IEvent : public IEntity {
public:
    IEvent() { _entityType = IENTITY_ID };
    virtual ~IEvent();
    void render(ACore &core) override {};
};