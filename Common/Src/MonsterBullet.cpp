//
// Created by kaww on 27/09/18.
//

#include <cmath>
#include <MonsterBullet.hpp>

#include "ActionManager.hpp"
#include "MonsterBullet.hpp"

MonsterBullet::MonsterBullet(sf::Vector2f &pos, float amplitude, std::string bullet) :
IBullet(pos, bullet),
_counter(0),
_originalY(pos.y),
_amplitude(amplitude)
{
    _entityType = MONSTERBULLET_ID;
}

MonsterBullet::~MonsterBullet()
{
}

void MonsterBullet::updateBullet(ACore &core)
{
    _counter += 0.2;

    _pos.x -= 4;
}

void MonsterBullet::onCollision(ACore &core, ISprite *sprite)
{
	(void)core;
    (void)sprite;
}

sf::Packet MonsterBullet::getEntityPacket(network::PACKET_TYPE packetType)
{
    sf::Packet packet;

    packet << packetType;
    if (packetType == network::ENT_UPDATE)
        packet << _id;
    packet << MONSTERBULLET_ID << _id << _pos.x << _pos.y << _amplitude << _originalY << _counter;
    return packet;
}

IEntity *NetworkConstructor::createMonsterBulletFromPacket(ACore &core, sf::Packet packet)
{
    int id;
    sf::Vector2f pos;
    float amplitude;

    packet >> id >> pos.x >> pos.y >> amplitude;
    IEntity *entity = new MonsterBullet(pos, amplitude);
    entity->setId(id);
    return entity;
}