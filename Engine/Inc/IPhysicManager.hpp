//
// Created by reimua on 25/11/18.
//

#pragma once

#include "ACore.hpp"
#include "ISprite.hpp"

/*
 * IPhysicManager is used to proc collision on sprites.
 *
 * detectCollision:
 * 		call the onCollision callback of two sprites whenever they enter in contact.
 */

class IPhysicManager {
public:
	IPhysicManager(){};
	virtual ~IPhysicManager(){};

	virtual void detectCollision(ACore&, std::vector<ISprite*>) = 0;
};