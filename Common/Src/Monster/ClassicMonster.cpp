//
// Created by kaww on 27/09/18.
//

#include "PlayerBullet.hpp"
#include "ActionManager.hpp"
#include "ClassicMonster.hpp"

ClassicMonster::ClassicMonster(sf::Vector2f &pos, float speed, float amplitude, float amplitudeSpeed, float scale) :
IMonster(pos, "./Assets/Sprites/ClassicMonster.png", speed, amplitude, amplitudeSpeed, scale),
_health(25)

{
    _entityType = CLASSICMONSTER_ID;
}

ClassicMonster::~ClassicMonster()
{
}

void ClassicMonster::updateMonster(ACore &core) {
    if (_health < 0) {
        core.addToDeletionQueue(this);
        auto monsterPos = sf::Vector2f(100, 0);
        float speed = (float)(std::rand() % 200) / 100;
        float amplitude = (float)(std::rand() % 1000) / 100;
        float amplitudeSpeed = (float)(std::rand() % 5) / 100;
        auto monster = new ClassicMonster(monsterPos,
                                              speed,
                                              amplitude,
                                              amplitudeSpeed, 4);
        core.feedEntity(monster);
    }
}

void ClassicMonster::onCollision(ACore &core, ISprite *sprite)
{
    if (sprite->getEntityType() == PLAYERBULLET_ID) {
		_health--;
        core.addToDeletionQueue(sprite);
    }
}

sf::Packet ClassicMonster::getEntityPacket(network::PACKET_TYPE packetType)
{
    sf::Packet packet;

    packet << packetType;
    if (packetType == network::ENT_UPDATE) {
        packet << _id;
    }
    packet << CLASSICMONSTER_ID << _id;
    packet << _pos.x << _pos.y << _amplitude << _speed << _amplitudeSpeed << _scale << _originalY << _counter;
    return packet;
}

void ClassicMonster::updateFromPacket(sf::Packet packet)
{
    int id;
    std::string texturePath;
    float posX;
    float posY;

    packet >> id;
    packet >> posX;
    packet >> posY;
    _pos.x = posX;
    _pos.y = posY;
}

IEntity *NetworkConstructor::createClassicMonsterFromPacket(ACore &core, sf::Packet packet)
{
    int id;
    sf::Vector2f pos;
    float amplitude;
    float speed;
    float amplitudeSpeed;
    float scale;
    float originalY;
    float counter;

    packet >> id >> pos.x >> pos.y >> amplitude >> speed >> amplitudeSpeed >> scale >> originalY >> counter;

    IMonster *entity = new ClassicMonster(pos, speed, amplitude, amplitudeSpeed, scale);
    entity->setOriginalY(originalY);
    entity->setCounter(counter);
    entity->setId(id);
    return entity;
}